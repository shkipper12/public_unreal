// Copyright Epic Games, Inc. All Rights Reserved.

#include "public_unreal.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, public_unreal, "public_unreal" );
 