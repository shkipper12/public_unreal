// Copyright Epic Games, Inc. All Rights Reserved.

#include "public_unrealGameMode.h"
#include "public_unrealHUD.h"
#include "public_unrealCharacter.h"
#include "UObject/ConstructorHelpers.h"

Apublic_unrealGameMode::Apublic_unrealGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = Apublic_unrealHUD::StaticClass();
}
