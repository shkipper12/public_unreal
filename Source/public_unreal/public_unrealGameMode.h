// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "public_unrealGameMode.generated.h"

UCLASS(minimalapi)
class Apublic_unrealGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	Apublic_unrealGameMode();
};



