// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "public_unrealHUD.generated.h"

UCLASS()
class Apublic_unrealHUD : public AHUD
{
	GENERATED_BODY()

public:
	Apublic_unrealHUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;

};

